General Instructions
================
--------------------------------

1. Clone this repo directly. 
2. Hardware is required for this project.
Use of Readme
============
------------------------------

* Use Table of Contents to navigate through the sections
* Review the edit log for recent changes
	* Remember to add you signature and date after you review recent changes!
* When you make a change, create an entry at the top of the Edit Log with the date of the change and your signature.

Links
====
----------

* [Atmel Microcontroller](http://www.atmel.com/products/microcontrollers/8051Architecture/default.aspx)
* [MASM](https://en.wikipedia.org/wiki/Microsoft_Macro_Assembler)

Table of Contents
==============
----------------------------------
[TOC]

Automatic Room Light Controller
=================================
----------------------------------------

Check the documentation in with the source files.

Edit Log
=======
-----------------
## Complete Creation
> Date: 28/11/13
> 
> **Sections:**
> 
> * General Instructions
> * Use of Readme
> * Links
> * Problem Statement
>
>
> **Read Signature**
> 
> * Siddhi Gupta: 28/11/13